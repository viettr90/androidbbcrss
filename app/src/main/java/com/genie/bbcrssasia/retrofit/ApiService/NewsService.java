package com.genie.bbcrssasia.retrofit.ApiService;

import com.genie.bbcrssasia.models.entity.AsiaNews;
import com.genie.bbcrssasia.models.entity.NewsHtml;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */
public interface NewsService {
    @GET("asia/rss.xml")
    Single<AsiaNews> getAsiaNews();

    @GET("{article}")
    Single<NewsHtml> getArticlePage(@Path("article") String article);

}
