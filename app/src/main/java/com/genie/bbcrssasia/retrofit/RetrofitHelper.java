package com.genie.bbcrssasia.retrofit;

import pl.droidsonroids.retrofit2.JspoonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */
public class RetrofitHelper {

    private static final String BASE_URL = "http://feeds.bbci.co.uk/news/world/";

    public static final String BASE_HTML_URL = "https://www.bbc.co.uk/news/";


    private static Retrofit retrofit = new Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
            .baseUrl(BASE_URL)
            .build();

    private static Retrofit retrofitHtml = new Retrofit.Builder()
            .baseUrl(BASE_HTML_URL)
            .addConverterFactory(JspoonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();



    public static Retrofit getInstance() {
        return retrofit;
    }

    public static Retrofit getInstanceForHtml() {
        return retrofitHtml;
    }

}
