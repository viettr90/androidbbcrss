package com.genie.bbcrssasia.models.dxo;

import com.genie.bbcrssasia.models.dto.NewsDto;
import com.genie.bbcrssasia.models.entity.AsiaNews;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */
public class NewsDxo {

    public static List<NewsDto> convertListNews(AsiaNews listNew) {
        List<NewsDto> newsDtoList = new ArrayList<>();
        for (AsiaNews.Channel.Item article : listNew.channel.itemList) {
            newsDtoList.add(convertNews(article));
        }
        return newsDtoList;
    }


    private static NewsDto convertNews(AsiaNews.Channel.Item news) {
        NewsDto newsDto = new NewsDto();

        newsDto.title = news.title;
        newsDto.description = news.description;
        newsDto.link = news.link;

        SimpleDateFormat std = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        SimpleDateFormat dts = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date pubdate = std.parse(news.pubDate);
            newsDto.pubdate = dts.format(pubdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        newsDto.thumbnailUrl = news.thumbnail.url;

        return newsDto;
    }
}
