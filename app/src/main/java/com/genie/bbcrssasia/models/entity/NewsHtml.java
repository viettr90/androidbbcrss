package com.genie.bbcrssasia.models.entity;

import pl.droidsonroids.jspoon.annotation.Selector;

/**
 * Created by viet.tr90@gmail.com on 1/22/19.
 */
public class NewsHtml {
    @Selector(".story-body__inner") public String body;
}
