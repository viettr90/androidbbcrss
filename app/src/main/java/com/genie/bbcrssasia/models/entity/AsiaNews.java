package com.genie.bbcrssasia.models.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */

@Root(name = "rss", strict = false)
public class AsiaNews {

    @Element(name = "channel")
    public Channel channel;

    public static class Channel {
        @Element(name = "title", required = false)
        public String title;
        @Element(name = "description", required = false)
        public String description;
        @Element(name = "link", required = false)
        public String link;
        @ElementList(name = "item", inline = true)
        public List<Item> itemList;

        @Root(name = "item", strict = false)
        public static class Item {
            @Element(name = "title", required = false)
            public String title;
            @Element(name = "description", required = false)
            public String description;
            @Element(name = "link", required = false)
            public String link;
            @Element(name = "pubDate", required = false)
            public String pubDate;
            @Element(name = "thumbnail", required = false)
            public Thumbnail thumbnail;

            @Root(name = "thumbnail", strict = false)
            public static class Thumbnail {
                @Attribute(name = "width")
                public int width;
                @Attribute(name = "height")
                public int height;
                @Attribute(name = "url")
                public String url;

            }
        }
    }
}
