package com.genie.bbcrssasia.models.dto;

import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Spanned;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */
public class NewsDto implements Parcelable {
    public String title;
    public String description;
    public String link;
    public String pubdate;
    public String thumbnailUrl;

    public Spanned body;

    public NewsDto() {

    }

    protected NewsDto(Parcel in) {
        title = in.readString();
        description = in.readString();
        link = in.readString();
        pubdate = in.readString();
        thumbnailUrl = in.readString();
    }

    public static final Creator<NewsDto> CREATOR = new Creator<NewsDto>() {
        @Override
        public NewsDto createFromParcel(Parcel in) {
            return new NewsDto(in);
        }

        @Override
        public NewsDto[] newArray(int size) {
            return new NewsDto[size];
        }
    };

    @BindingAdapter({"bind:thumbnailUrl"})
    public static void loadImage(ImageView view, String thumbnailUrl) {
        Picasso.get()
                .load(thumbnailUrl)
                .into(view);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(link);
        parcel.writeString(pubdate);
        parcel.writeString(thumbnailUrl);
    }
}
