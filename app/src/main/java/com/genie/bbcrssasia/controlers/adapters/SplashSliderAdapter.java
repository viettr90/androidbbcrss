package com.genie.bbcrssasia.controlers.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.genie.bbcrssasia.R;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */
public class SplashSliderAdapter extends PagerAdapter {
    private String[] descriptions;


    private LayoutInflater mLayoutInflater;

    public SplashSliderAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
        descriptions = context.getResources().getStringArray(R.array.tutorial_descriptions);

    }

    @Override
    public int getCount() {
        return descriptions == null ? 0 : descriptions.length;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View item = mLayoutInflater.inflate(R.layout.tutorial_screen, container, false);

        TextView desTv = item.findViewById(R.id.tutorial_description);
        if (descriptions != null) {
            desTv.setText(descriptions[position]);
        }

        container.addView(item);

        return item;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
