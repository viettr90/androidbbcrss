package com.genie.bbcrssasia.controlers.activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.genie.bbcrssasia.R;
import com.genie.bbcrssasia.databinding.ActivityNewsDetailBinding;
import com.genie.bbcrssasia.models.dto.NewsDto;
import com.genie.bbcrssasia.models.entity.NewsHtml;
import com.genie.bbcrssasia.retrofit.ApiService.NewsService;
import com.genie.bbcrssasia.retrofit.RetrofitHelper;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.genie.bbcrssasia.retrofit.RetrofitHelper.BASE_HTML_URL;

public class NewsDetailActivity extends AppCompatActivity {

    private ActivityNewsDetailBinding binding;
    private NewsDto newsDto;
    private CompositeDisposable mDisposable = new CompositeDisposable();
    private Button retryBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_news_detail);
        initView();
        setupData();
    }

    private void initView() {
        // toolbar
        Toolbar toolbar = findViewById(R.id.activity_news_detail_toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        retryBtn = findViewById(R.id.retry);

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupData();
                retryBtn.setVisibility(View.GONE);
            }
        });
    }

    private void setupData() {
        findViewById(R.id.activity_news_detail_progress).setVisibility(View.VISIBLE);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            newsDto = bundle.getParcelable(NewsDto.class.getCanonicalName());
            NewsService newsService = RetrofitHelper.getInstanceForHtml().create(NewsService.class);
            Single<NewsHtml> newsHtmlSingle = newsService.getArticlePage(newsDto.link.replace(BASE_HTML_URL, ""));
            newsHtmlSingle.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<NewsHtml>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(NewsHtml newsHtml) {
                            newsDto.body = Html.fromHtml(newsHtml.body);
                            binding.setNews(newsDto);
                            findViewById(R.id.activity_news_detail_progress).setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Throwable e) {
                            findViewById(R.id.activity_news_detail_progress).setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();
                            retryBtn.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDisposable.dispose();
    }
}
