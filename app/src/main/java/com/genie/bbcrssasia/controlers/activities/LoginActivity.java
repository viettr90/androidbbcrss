package com.genie.bbcrssasia.controlers.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.genie.bbcrssasia.R;
import com.genie.bbcrssasia.preferences.LoginPreference;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String DEFAULT_USER = "GoBear";
    private static final String DEFAULT_PASS = "GoBearDemo";

    private LoginPreference mLoginPreference;

    private EditText mUserName;
    private EditText mPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewById(R.id.login_btn).setOnClickListener(this);
        mUserName = findViewById(R.id.login_user_name);
        mPassword = findViewById(R.id.login_password);

        mLoginPreference = new LoginPreference(getApplicationContext());

        // If user've already login and using rememberMe feature
        if (mLoginPreference.isLogin() && mLoginPreference.isAutoLogin) {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), NewsListActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                findViewById(R.id.login_progress).setVisibility(View.VISIBLE);
                // Demo login process
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isUser(mUserName.getText().toString(), mPassword.getText().toString())) {

                            // Update Login preference
                            mLoginPreference.userId = mUserName.getText().toString(); // user Id should be got from server
                            mLoginPreference.token = mPassword.getText().toString(); // token should be got from server
                            SwitchCompat rememberSwitch = findViewById(R.id.login_remember);
                            mLoginPreference.isAutoLogin = rememberSwitch.isChecked();

                            // Open NewsList screen
                            Intent intent = new Intent();
                            intent.setClass(getApplicationContext(), NewsListActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        mLoginPreference.write();

                        findViewById(R.id.login_progress).setVisibility(View.GONE);
                    }
                }, 1000);
                break;
            case R.id.login_remember:

                break;
        }
    }

    private boolean isUser(String username, String pass) {
        return TextUtils.equals(username, DEFAULT_USER)
                && TextUtils.equals(pass, DEFAULT_PASS);
    }
}
