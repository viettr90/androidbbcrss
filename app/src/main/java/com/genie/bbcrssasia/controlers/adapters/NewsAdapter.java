package com.genie.bbcrssasia.controlers.adapters;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.genie.bbcrssasia.BR;
import com.genie.bbcrssasia.R;
import com.genie.bbcrssasia.models.dto.NewsDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */
public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_NEWS = 1;

    private List<NewsDto> newsDtoList;

    private NewsAdapterListener mListener;

    public NewsAdapter(NewsAdapterListener listener) {
        mListener = listener;
        newsDtoList = new ArrayList<>();
    }

    public void addAll(List<NewsDto> newsDtos) {
        newsDtoList.clear();
        // This is mock item for header
        newsDtoList.add(new NewsDto());
        newsDtoList.addAll(newsDtos);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            View view = layoutInflater.inflate(R.layout.gobear_header, parent, false);
            return new HeaderHolder(view);
        } else {
            ViewDataBinding binding = DataBindingUtil.inflate(
                    layoutInflater, R.layout.news_list_item, parent, false);
            return new NewsHolder(binding);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (getItemViewType(position) != TYPE_HEADER) {
            Object obj = newsDtoList.get(position);
            ((NewsHolder)holder).bind(obj);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onNewsClicked(newsDtoList.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return newsDtoList == null ? 0 : newsDtoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_NEWS;
    }

    private boolean isHeader(int position) {
        return position == 0;
    }

    class HeaderHolder extends RecyclerView.ViewHolder {
        TextView logout;
        HeaderHolder(View itemView) {
            super(itemView);
            logout = itemView.findViewById(R.id.logout);
            logout.setVisibility(View.VISIBLE);
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onLogoutClicked();
                }
            });
        }
    }

    class NewsHolder extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        NewsHolder(final ViewDataBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }

        void bind(Object obj) {
            binding.setVariable(BR.news, obj);
            binding.executePendingBindings();
        }
    }


    public interface NewsAdapterListener {
        void onLogoutClicked();
        void onNewsClicked(NewsDto news);
    }

}
