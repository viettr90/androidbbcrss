package com.genie.bbcrssasia.controlers.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.genie.bbcrssasia.R;
import com.genie.bbcrssasia.controlers.adapters.NewsAdapter;
import com.genie.bbcrssasia.models.dto.NewsDto;
import com.genie.bbcrssasia.models.dxo.NewsDxo;
import com.genie.bbcrssasia.models.entity.AsiaNews;
import com.genie.bbcrssasia.preferences.LoginPreference;
import com.genie.bbcrssasia.retrofit.ApiService.NewsService;
import com.genie.bbcrssasia.retrofit.RetrofitHelper;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class NewsListActivity extends AppCompatActivity implements NewsAdapter.NewsAdapterListener {

    private CompositeDisposable mDisposable = new CompositeDisposable();
    private NewsAdapter mNewsAdapter;
    private Button retryBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        initView();
        fetchAsiaNews();
    }

    private void initView() {

        /* Setup RecyclerView */
        RecyclerView mRecyclerView = findViewById(R.id.news_list_recycler);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mNewsAdapter = new NewsAdapter(this);
        mRecyclerView.setAdapter(mNewsAdapter);

        /* Setup SwipeRefreshLayout */
        final SwipeRefreshLayout refreshLayout = findViewById(R.id.news_list_refresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(false);
                fetchAsiaNews();
            }
        });

        retryBtn = findViewById(R.id.retry);

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchAsiaNews();
                retryBtn.setVisibility(View.GONE);
            }
        });

    }

    private void fetchAsiaNews() {

        findViewById(R.id.news_list_progress).setVisibility(View.VISIBLE);

        Retrofit retrofit = RetrofitHelper.getInstance();
        NewsService newsService = retrofit.create(NewsService.class);
        // Register observable
        Single<AsiaNews> mListAsiaNews = newsService.getAsiaNews();

        mListAsiaNews.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<AsiaNews>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(AsiaNews asiaNews) {
                        Log.d("GENIE", "onSuccess: " + asiaNews);

                        List<NewsDto> newsDtoList = NewsDxo.convertListNews(asiaNews);
                        mNewsAdapter.addAll(newsDtoList);

                        findViewById(R.id.news_list_progress).setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("GENIE", "onError: " + e);
                        Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();
                        retryBtn.setVisibility(View.VISIBLE);
                    }
                });
    }

    boolean isExit;
    @Override
    public void onBackPressed() {
        if (isExit) {
            super.onBackPressed();
        } else {
            isExit = true;
            Toast.makeText(this, "Back again for exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isExit = false;
                }
            }, 1000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDisposable.dispose();
    }

    @Override
    public void onLogoutClicked() {
        finishAffinity();

        LoginPreference loginPreference = new LoginPreference(getApplicationContext());
        loginPreference.userId = "";
        loginPreference.token = "";
        loginPreference.write();

        Intent intent = new Intent();
        intent.setClass(getApplicationContext() ,LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onNewsClicked(NewsDto news) {
        Intent intent = new Intent();
        intent.setClass(this.getApplicationContext(), NewsDetailActivity.class);
        intent.putExtra(NewsDto.class.getCanonicalName(), news);
        startActivity(intent);
    }
}
