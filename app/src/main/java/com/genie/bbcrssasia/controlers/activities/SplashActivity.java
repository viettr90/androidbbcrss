package com.genie.bbcrssasia.controlers.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.genie.bbcrssasia.R;
import com.genie.bbcrssasia.controlers.adapters.SplashSliderAdapter;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by viet.tr90@gmail.com on 1/21/19.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new SplashSliderAdapter(getApplicationContext()));

        final CircleIndicator indicator = findViewById(R.id.splash_indicator);
        indicator.setViewPager(viewPager);

        findViewById(R.id.skip_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

    }
}
