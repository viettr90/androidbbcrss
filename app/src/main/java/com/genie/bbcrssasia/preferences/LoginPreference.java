package com.genie.bbcrssasia.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class LoginPreference extends BasePreference {

    private static String PREFERENCE_NAME = "login_preference";

    private static String KEY_USER_ID = "user_id";

    private static String KEY_TOKEN = "token";

    private static String KEY_AUTO_LOGIN = "auto_login";


    public String userId;

    public String token;

    public boolean isAutoLogin;


    public LoginPreference(Context context) {
        super();
        mContext = context;
        init();
    }

    @Override
    protected String getPreferenceName() {
        return PREFERENCE_NAME;
    }

    @Override
    protected void setData(SharedPreferences.Editor editor) {
        editor.putString(KEY_USER_ID, userId);
        editor.putString(KEY_TOKEN, token);
        editor.putBoolean(KEY_AUTO_LOGIN, isAutoLogin);
    }

    @Override
    protected void getData(SharedPreferences preferences) {
        userId = preferences.getString(KEY_USER_ID, null);
        token = preferences.getString(KEY_TOKEN, null);
        isAutoLogin = preferences.getBoolean(KEY_AUTO_LOGIN, false);

    }

    public boolean isLogin() {
        return !TextUtils.isEmpty(token);
    }
}
