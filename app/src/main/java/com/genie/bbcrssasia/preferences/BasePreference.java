package com.genie.bbcrssasia.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class BasePreference {

    /**
     * Application Context
     */
    Context mContext;

    private SharedPreferences mPreferences;

    private SharedPreferences.Editor mEditor;

    synchronized void init() {
        mPreferences = mContext.getSharedPreferences(getPreferenceName(), Context.MODE_PRIVATE);
        getData(mPreferences);
    }

    public synchronized void write() {
        mEditor = mPreferences.edit();
        setData(mEditor);
        mEditor.apply();
    }

    protected abstract String getPreferenceName();

    protected abstract void setData(SharedPreferences.Editor editor);

    protected abstract void getData(SharedPreferences preferences);

}
