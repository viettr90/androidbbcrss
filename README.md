# Android BBC rss Application

## Installation
Clone this repository and import into **Android Studio**
```bash
git clone https://viettr90@bitbucket.org/viettr90/androidbbcrss.git
```

## Configuration
- `Open Android Studio`
- `File -> Open -> cloned project's folder`
- 'Make sure Gradle online mode (Android Studio -> Preference... -> Gradle -> uncheck "Offline work")'

## Build and Open app on device
1. Connect device with your laptop
2. Make sure you already enable developer mode on your device and checked USB debuging
From Android Studio:
3. Run -> run app
4. Choose your device -> OK.

## Launch App

1. Press "Skip" button to skip tutorial screen
2. Login with below info
  •  Username: GoBear
  •  Password: GoBearDemo
3. Check app
